from django.shortcuts import render, redirect
from .models import Profile

from main.forms import ProfileForm


def index(request):
    return render(request, 'main/index.html')


def profile(request):
    if request.method == 'POST':
        form = ProfileForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('profile')
    
    form = ProfileForm()
    profiles = Profile.objects.order_by('-id')[:1]
    
    return render(request, 'main/layout_dop/profile.html', {'form': form, 'profiles': profiles})