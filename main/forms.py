from .models import Profile
from django.forms import EmailInput, ModelForm, NumberInput, TextInput


class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ["first_name", "last_name", "email", "phone_number", "data_day", "data_month", "data_year"]
        
        widgets = {"first_name": TextInput(attrs={
            'class': 'information__input',
        }),
        "last_name": TextInput(attrs={
            'class': 'information__input',
        }),
        "email": EmailInput(attrs={
            'class': 'information__input',
        }),
        "phone_number": TextInput(attrs={
            'class': 'information__input',
        }),
        "data_day": NumberInput(attrs={
            'class': 'information__input',
        }),
        "data_month": NumberInput(attrs={
            'class': 'information__input',
        }),
        "data_year": NumberInput(attrs={
            'class': 'information__input',
        }),
        }
