from datetime import datetime

from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


class Profile(models.Model):
    first_name = models.CharField('firsName', max_length=50)
    last_name = models.CharField('lastName', max_length=50)
    email = models.EmailField('email', max_length=50)
    phone_number = models.CharField('phoneNumber', max_length=50)
    data_day = models.IntegerField('dataDay', default=datetime.now().day, validators = [MaxValueValidator(31), MinValueValidator(1)], blank=True)
    data_month = models.IntegerField('dataMonth', default=datetime.now().month, validators = [MaxValueValidator(12), MinValueValidator(1)], blank=True)
    data_year = models.IntegerField('dataYear', default=datetime.now().year, validators = [MaxValueValidator(datetime.now().year), MinValueValidator(1800)], blank=True)
    

    def __str__(self):
        template = '{0.first_name} {0.last_name} {0.email} {0.phone_number} {0.data_day} {0.data_month} {0.data_year}'
        return template.format(self)

    class Meta:
        verbose_name = "Profile"
        verbose_name_plural = "Profiles"